import time
from collections import deque
# Import the ADS1x15 module.
import Adafruit_ADS1x15
import socket
import struct

# Create an ADS1115 ADC (16-bit) instance.
adc = Adafruit_ADS1x15.ADS1115()

# Note you can change the I2C address from its default (0x48), and/or the I2C
# bus by passing in these optional parameters:
#adc = Adafruit_ADS1x15.ADS1015(address=0x49, busnum=1)

# Choose a gain of 1 for reading voltages from 0 to 4.09V.
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
GAIN = 2/3

# Print nice channel column headers.
#print('| {0:>6} | {1:>6} | {2:>6} | {3:>6} |'.format(*range(4)))
#print('-' * 37)

#q_x = deque([])
#q_y = deque([])
q_z = deque([])
i = 0
q_depth = 50

#max_x = 8600 
#max_y = 8600 
max_z = 8800

#min_x = 8800
#min_y = 8800
min_z = 8699

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
addr = ("192.168.1.111", 12000)

while True:

    if q_depth > 0:
        q_depth -= 1
    else:
        #q_x.pop()
        #q_y.pop()
        q_z.pop()

    #x = adc.read_adc(0, gain=GAIN)
    #y = adc.read_adc(1, gain=GAIN)
    z = adc.read_adc(2, gain=GAIN)


    #max_x = max_x + 1 if x > max_x else max_x
    #max_y = max_y + 1 if y > max_y else max_y
    #max_z = max_z + 1 if z > max_z else max_z
    max_z = max_z + (z-max_z) if z > max_z else max_z
    #max_z -= 1 if max_z > 8700 else max_z
    #if max_z > 8810: max_z -= 10

    # don't set min if it collides with max
    #min_x = min_x - 1 if x < min_x else min_x
    #min_y = min_y - 1 if y < min_y else min_y
    #min_z = min_z - 1 if z < min_z else min_z
    min_z = min_z - (min_z-z) if z < min_z else min_z
    #min_z += 1 if min_z < 8699 else min_z
    #if min_z < 8689: min_z += 10

    # z will be the at rest value (likely midpoint)
    # ranges 0-100% w/ ~50% being rest
    #proportion = round(float(z-min_z)/(max_z-min_z),2)

    #q_x.appendleft(x)
    #q_y.appendleft(y)
    q_z.appendleft(z)


    #q_x_stat = sum(q_x)/len(q_x)
    #q_y_stat = sum(q_y)/len(q_y)
    q_z_stat = sum(q_z)/len(q_z)

    #print ("{}-{}-{} {}".format(min_z, z, max_z, proportion))
    #print("{} {} {}".format(min_z, round(q_z_stat), max_z))
    #print("{}".format(round(q_z_stat)-z))
    #print("{}".format( round((q_z_stat-z)/(max_z-min_z),2) ))
    proportion = round((q_z_stat-z)/(max_z-min_z),2)
    if proportion > 0.01:
        #print ("{} Beat".format(proportion))
        bytes = struct.pack('h', 1)
        print (bytes)
        client_socket.sendto(bytes, addr)

    #q_x_stat = max(q_x)
    #q_y_stat = max(q_y)
    #q_z_stat = max(q_z)

    #print('| {0:>6} | {1:>6} | {2:>6}'.format(int(x-q_x_avg),int(y-q_y_avg),int(z-q_z_avg)))

    # avoid div zero
    #if x and y and z:
    if 0:
        #print("{} {} {}".format(x,y,z)) 
        #print("{}".format(z)) 
        #bytes = struct.pack('hhh', int((x/max_x)*100), int((y/max_y)*100), int((z/max_z)*100))
        #bytes = struct.pack('hhh', x-8900, y-8870, z-8900)
        bytes = struct.pack('h', z-8900)
        #print (bytes)
        client_socket.sendto(bytes, addr)

