#!/usr/bin/env python3
"""Ultra simple sample on how to use the library"""
from driver import apa102
import time
import random
import sys
import math

# Initialize the library and the strip
strip = apa102.APA102(num_led=6, global_brightness=20, mosi = 10, sclk = 11,
                                  order='rbg')

# Turn off all pixels (sometimes a few light up when the strip gets power)
strip.clear_strip()

red = 0x03FF03
brightness = 1
brightness_level = 0

strip.set_pixel_rgb(0, random.randint(0, 0x00ffff) )
strip.set_pixel_rgb(1, random.randint(0, 0x00ffff) )
strip.set_pixel_rgb(2, random.randint(0, 0xffffff) )
strip.set_pixel_rgb(3, random.randint(0, 0xffffff) )
strip.set_pixel_rgb(4, random.randint(0, 0xffffff) )
strip.set_pixel_rgb(5, random.randint(0, 0xffffff) )
strip.show()


for value in range(100,100000):
    # Prepare a few individual pixels
    strip.set_pixel_rgb(0, red, brightness_level)
    strip.set_pixel_rgb(1, red, brightness_level)
    if value % 20 == 0:
        strip.set_pixel_rgb(2, random.randint(0, 0xffffff) )
        strip.set_pixel_rgb(3, random.randint(0, 0xffffff) )
        strip.set_pixel_rgb(4, random.randint(0, 0xffffff) )
        strip.set_pixel_rgb(5, random.randint(0, 0xffffff) )
        # GRB
        pass

    # Copy the buffer to the Strip (i.e. show the prepared pixels)
    strip.show()
    time.sleep(.01)

    brightness += .025
    brightness_level = ((math.sin(brightness)+1)/2)*100

# Wait a few Seconds, to check the result
time.sleep(0)

# Clear the strip and shut down
strip.clear_strip()
strip.cleanup()


